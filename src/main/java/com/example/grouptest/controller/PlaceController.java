package com.example.grouptest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PlaceController {
    @GetMapping("/")
    public String list() {
        return "group-test";
    }
}
