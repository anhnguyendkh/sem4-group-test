package com.example.grouptest.api;

import com.example.grouptest.entity.Place;
import com.example.grouptest.service.PlaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/places")
@Slf4j
@RequiredArgsConstructor
public class PlaceAPI {
    private static String UPLOADED_FOLDER = "target/classes/static/uploaded/";

    private final PlaceService placeService;

    @GetMapping
    public ResponseEntity<List<Place>> findAll() {
        return ResponseEntity.ok(placeService.findAll());
    }

    @PostMapping("/upload-image")
    public String uploadImage(@Valid @RequestParam("img") MultipartFile image) {

        try {
            byte[] bytes = image.getBytes();
            UUID uuid = UUID.randomUUID();
            Path path = Paths.get(UPLOADED_FOLDER + uuid + '-' + image.getOriginalFilename());
            Files.write(path, bytes);
            String urlImage =  "/uploaded/" + uuid  + '-' + image.getOriginalFilename();
            return urlImage;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Place place) {
        return ResponseEntity.ok(placeService.save(place));
    }

}
