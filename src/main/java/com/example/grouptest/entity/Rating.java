package com.example.grouptest.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private float rate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "placeId", referencedColumnName = "id")
    private Place place;
}
