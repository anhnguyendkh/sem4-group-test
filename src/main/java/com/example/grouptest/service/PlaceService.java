package com.example.grouptest.service;

import com.example.grouptest.entity.Place;
import com.example.grouptest.repository.PlaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaceService {
    private final PlaceRepository placeRepository;

    public List<Place> findAll() {
        return placeRepository.findAll();
    }

    public Place save(Place place) {
        return placeRepository.save(place);
    }
}
