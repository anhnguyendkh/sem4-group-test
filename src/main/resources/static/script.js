var places = [];
function findPlace(placeId) {
    return place[findPlaceKey(placeId)];
}
function findPlaceKey(placeId) {
    for (var key = 0; key < places.length; key++) {
        if (places[key].id = placeId) {
            return key;
        }
    }
}

var placeService = {
    findAll(fn) {
        axios
            .get('/api/v1/places')
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    create(place, fn) {
        axios
            .post('/api/v1/places', place)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    uploadImage(data) {
        return axios
            .post('/api/v1/places/upload-image', data)
            .then(response => response)
            .catch(error => console.log(error))
    }
}

var ListPlace = Vue.extend({
    template: "#place-list",
    data: function() {
        return { places: [] };
    },
    computed: {
        filteredPlaces() {
            return this.places;
        }
    },
    mounted() {
        placeService.findAll(r => { this.places = r.data; places = r.data });
    }
});

var AddPLace = Vue.extend({
    template: '#add-place',
    data() {
        return {
            place: { name: '', information: '', imageUrl: '' }
        }
    },
    methods: {
        changeImage: async function(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length) {
                return;
            }
            let data = new FormData();
            data.append('img', e.target.files[0]);
            const res = await placeService.uploadImage(data);
            this.place.imageUrl = res.data;
        },
        createPlace: function () {
            placeService.create(this.place)
        }
    }
});

var router = new VueRouter({
    routes: [
        { path: '/', component: ListPlace },
        { path: '/add-place', component: AddPLace }
    ]
});
new Vue({
    router
}).$mount('#app')